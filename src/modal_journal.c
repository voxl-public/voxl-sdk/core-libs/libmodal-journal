/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <voxl_cutils.h>

#include "modal_journal.h"

// Items in the volatile dir get wiped on reboot, persistent do not
#define VOLATILE_DIR "/run/mjrn/"
#define PERSIST_DIR  "/data/mjrn/"
#define BOOT_DIR     PERSIST_DIR"current-boot/"
#define BOOT_LOCK    VOLATILE_DIR"current-boot-ready"
#define LEVEL_FILE   PERSIST_DIR"log-level"
#define LEVEL_DIR    PERSIST_DIR"log-level.d/"
static char VERBOSE_FILE[128];
static char DEBUG_FILE[128];

// We're scanning the filesystem continuously, do so every N seconds
#define SCAN_INTERVAL_SEC 1

static char PROCESS_NAME[64];
FILE *logfile;

static M_JournalLevel userDebugLevel    = WARNING;
static M_JournalLevel fsDebugLevel      = WARNING;
#define currentDebugLevel (userDebugLevel > fsDebugLevel ? fsDebugLevel : userDebugLevel)

static pthread_mutex_t print_mutex = PTHREAD_MUTEX_INITIALIZER;

static int keep_scanning = 1;
static pthread_cond_t scan_cond = PTHREAD_COND_INITIALIZER;
static pthread_t fs_scan_thread;

static void print_level(M_JournalLevel level, FILE *stream);
static int _file_exists(char* path);
static int _dir_exists(char* path);

// Has to be up top because gcc whines about void/no args function prototypes :(
static void updateFSLevel()
{
    M_JournalLevel debugLevel = WARNING;
    char buffer[256];
    static int first_run = 1;

    if(_file_exists(LEVEL_FILE)){ //Check for systemwide flag
        FILE *file = fopen(LEVEL_FILE, "r");
        fgets(buffer, 64, file);
        strtok(buffer, "\n");
        fclose(file);

        //To lower
        for (size_t i = 0; i < strlen(buffer); ++i) {
            if (buffer[i] >= 'A' && buffer[i] <= 'Z') buffer[i] -= ('A' - 'a');
        }

        unsigned int i;
        //Support integer or string in file
        if(sscanf(buffer, "%u", &i) == 1){
            if(i <= PRINT)
                debugLevel = i;
            else
                if(first_run) M_WARN("Invalid log level in log-level file: %s\n", buffer);
        } else if (!strcmp(buffer, "verbose")) {
            debugLevel = VERBOSE;
        } else if (!strcmp(buffer, "debug")) {
            debugLevel = DEBUG;
        } else if (!strcmp(buffer, "warning")) {
            debugLevel = WARNING;
        } else if (!strcmp(buffer, "error")) {
            debugLevel = ERROR;
        } else if (!strcmp(buffer, "print")) {
            debugLevel = PRINT;
        } else {
            if(first_run) M_WARN("Invalid log level in log-level file: %s\n", buffer);
        }
    }

    // ------------------ Get Default level from filesystem -------------------
    if(_file_exists(VERBOSE_FILE)){ //Verbose if process specific verbose file exists
        debugLevel = VERBOSE;
    } else if(_file_exists(DEBUG_FILE)){ //Debug if process specific debug file exists
        if(debugLevel > DEBUG) debugLevel = DEBUG;
    }

    fsDebugLevel = debugLevel;

    first_run = 0;
}

static void *scan_thread_func(__attribute__((unused)) void* data){
    pthread_mutex_t dummy_mutex = PTHREAD_MUTEX_INITIALIZER;

    while(keep_scanning){
        updateFSLevel();

        // We really just want to sleep(3) but can't have the destructor block for that long
        struct timespec ts;
        clock_gettime(CLOCK_REALTIME, &ts);
        ts.tv_sec += SCAN_INTERVAL_SEC;
        pthread_cond_timedwait(&scan_cond, &dummy_mutex, &ts);
    }

    pthread_mutex_destroy(&dummy_mutex);
    return NULL;
}

static __attribute__((constructor)) void journal_constructor()
{
    char buffer[256];
    char dirbuffer[256];

    // ------------------------- Get own process name -------------------------
    FILE *file = fopen("/proc/self/cmdline", "r");
    fgets(PROCESS_NAME, 64, file);
    strtok(PROCESS_NAME, " ");
    strtok(PROCESS_NAME, "\n");
    fclose(file);
    snprintf(VERBOSE_FILE, 127, "%s%s.verbose", LEVEL_DIR, PROCESS_NAME);
    snprintf(DEBUG_FILE,   127, "%s%s.debug",   LEVEL_DIR, PROCESS_NAME);

    // -------------------------- Check current boot --------------------------
    // Check for bootfile existence
    if(!_file_exists(BOOT_LOCK)){
        M_WARN("Could not find boot lock file: \"%s\", \
            \n\tfilesystem logging will not be enabled for this run, \
            \n\tMake sure that the voxl-wait-for-fs service has completed for fs logging", BOOT_LOCK);
        return;
    }

    // ------------- Make sure we have a folder for this process --------------
    snprintf(dirbuffer, 255, "%s%s", BOOT_DIR, PROCESS_NAME);
    if(!_dir_exists(dirbuffer)){
        mkdir(dirbuffer, 0700);
    }

    // ------------------------ Setup this log's file -------------------------
    for(int i=0;;i++){
        // construct a new path with the dir, name, and index i
        snprintf(buffer, 255, "%s/log-%04d.log", dirbuffer, i);
        if(!_file_exists(buffer)){
            // name with this index doesn't exist yet, good, use it!
            logfile = fopen(buffer, "w");
            break;
        }
    }

    updateFSLevel();

    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    pthread_create(&fs_scan_thread, &attr, &scan_thread_func, NULL);
    pthread_attr_destroy(&attr);
}

static __attribute__((destructor)) void journal_destructor()
{
    keep_scanning = 0;
    pthread_cond_broadcast(&scan_cond);
    pthread_join(fs_scan_thread, NULL);

    pthread_cond_destroy(&scan_cond);
    pthread_mutex_destroy(&print_mutex);

    if(logfile) fclose(logfile);
}

void M_JournalSetLevel(M_JournalLevel level)
{
    userDebugLevel = level;
}

void M_JournalPrint(M_JournalLevel level, const char * format, ...)
{
    // Only print for the current debug level
    if (level < currentDebugLevel) return;

    pthread_mutex_lock(&print_mutex);

    FILE* stream;
    switch (level) {
        case ERROR:
        case WARNING:
            stream = stderr;
            break;

        default:
            stream = stdout;
            break;
    }

    va_list args;
    va_start(args, format);

    print_level(level, stream);
    vfprintf(stream,  format, args);
    fflush(stream);

    if(logfile){
        print_level(level, logfile);
        vfprintf(logfile, format, args);
        fflush(logfile);
    }

    va_end(args);

    pthread_mutex_unlock(&print_mutex);
}

static void print_level(M_JournalLevel level, FILE *stream)
{
    // Check if we're printing to a terminal or not
    if(isatty(fileno(stream))){
        //Print with colors to terminal
        switch (level) {
            case VERBOSE:
                fprintf(stream, FONT_BOLD COLOR_GRN     "VERBOSE: " RESET_FONT);
                break;
            case DEBUG:
                fprintf(stream, FONT_BOLD COLOR_LIT_BLU "DEBUG:   " RESET_FONT);
                break;
            case WARNING:
                fprintf(stream, FONT_BOLD COLOR_YLW     "WARNING: " RESET_FONT);
                break;
            case ERROR:
                fprintf(stream, FONT_BOLD COLOR_RED     "ERROR:   " RESET_FONT);
                break;
            default:
                //Do Nothing
                break;
        }
    } else {
        switch (level) {
            case VERBOSE:
                fprintf(stream,"VERBOSE: ");
                break;
            case DEBUG:
                fprintf(stream,"DEBUG:   ");
                break;
            case WARNING:
                fprintf(stream,"WARNING: ");
                break;
            case ERROR:
                fprintf(stream,"ERROR:   ");
                break;
            default:
                //Do Nothing
                break;
        }
    }
}

static int _file_exists(char* path)
{
    // file exists
    if(access(path, F_OK ) != -1 ) return 1;
    // file doesn't exist
    return 0;
}

static int _dir_exists(char *path)
{
    struct stat sb;
    if (stat(path, &sb) == 0 && S_ISDIR(sb.st_mode)) {
        return 1;
    }

    return 0;
}
